//require dependencies
const express = require('express');
const app = express();
let config = require('./config');
let TodoListRoutes = require('./routes/todoListRoute');
var logger = require('morgan') // logger middleware 
var bodyParser = require('body-parser');

app.use(logger('dev'));
// app.use(bodyParser.json());


app.get('/', function(req, res) {
    res.send('Welcome to the Hello World API!');
});

app.use('/getTodoList', TodoListRoutes);// Inicializa  las rutas  de todo list


 // catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    err.name = "Error Page Not Found";

    // res.send(`Error Page Not Found `);
    res.send(err);
});



// error handler
app.use(function (err, req, res, next) {
    res.status(500);
    res.render('Something was wrong', { error: err });
});


app.listen(config.port, function() {
    console.log(`Example app listening on port ${config.port}!`);
});

//log to console to let us know it's working
console.log('Kushy API server started on: ' + config.port);