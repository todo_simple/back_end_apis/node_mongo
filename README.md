   http://stayregular.net/blog/make-a-nodejs-api-with-mysql

   npm init  --- para inicializar un nuevo proyecto


 - express - MVC for creating Node sites
 - express-rate-limit - Allows for rate limiting of API
 - cors - Cors will allow you to serve the API remotely
 - helmet - Secures your Express app with HTTP headers
 - mysql - Connects and interacts with MySQL through Node.


 - nodemon -Nodemon is used for hot reloading the server in development. Whenever we make a change in the code and save, if nodemon is running, it'll restart the Node server with the new code.



 Our app is structured as MVC, or Model View Controller
 * The model is the connection to the MySQL db.
 * The view are the routes we connect to and display JSON data
 * The controller are the functions that get data from the model and feed it to the view. 